import { MikroORM } from '@mikro-orm/core'
import { ApolloServer } from 'apollo-server-express'
import express from 'express'
import 'reflect-metadata'
import { buildSchema } from 'type-graphql'
import microConfig from './mikro-orm.config'
import { PostResolver } from './resolvers/post'
import { TestResolver } from './resolvers/test'
import { UserResolver } from './resolvers/user'

const main = async () => {
  // Initialize config with MikroORM config
  const orm = await MikroORM.init(microConfig)

  // Automatically run migration
  await orm.getMigrator().up()

  const app = express()

  // Access EntityManager object from ORM
  const { em } = orm

  // Start graphql server
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [TestResolver, PostResolver, UserResolver],
      validate: false,
    }),
    // Send EntityManager to context
    // To be accesed by resolvers
    context: () => ({ em }),
  })

  // Create /graphql endpoint on express
  apolloServer.applyMiddleware({ app })

  app.listen(4000, () => {
    console.log('server started on localhost:4000')
  })
}

main().catch((err) => {
  console.error(err)
})
