import { MikroORM } from '@mikro-orm/core'
import { __prod__ } from './constants'
import { Post } from './entities/Post'
import path from 'path'
import { user, password } from './../secret'
import { User } from './entities/User'

export default {
  migrations: {
    // path to the folder with migrations
    path: path.join(__dirname, 'migrations'),
    // regex pattern for the migration files JS or TS
    pattern: /^[\w-]+\d+\.[tj]s$/,
  },
  entities: [Post, User],
  dbName: 'lireddit',
  type: 'postgresql',
  user,
  password,
  debug: !__prod__,
  //   Send first parameter MikroORM requires
} as Parameters<typeof MikroORM.init>[0]
