import { Arg, Ctx, Mutation, Query, Resolver } from 'type-graphql'
import { Post } from '../entities/Post'
import { MyContext } from '../types'

@Resolver()
export class PostResolver {
  // QUERIES

  // Add graphQL type
  @Query(() => [Post])
  // returns array of all Post
  posts(@Ctx() { em }: MyContext): Promise<Post[]> {
    return em.find(Post, {})
  }

  // Add graphQL type
  // Can be Post or null
  @Query(() => Post, { nullable: true })
  // returns array of all Post
  post(
    // Modify Schema query in @Arg 'id'
    // Infers type-graphql infers Int type
    @Arg('id') id: number,
    // Do a TS union <Post | null>
    @Ctx()
    { em }: MyContext
  ): Promise<Post | null> {
    // Lookup based on id
    return em.findOne(Post, { id })
  }

  // MUTATIONS

  // Create post with Arg
  @Mutation(() => Post)
  async createPost(
    @Arg('title') title: string,
    @Ctx() { em }: MyContext
  ): Promise<Post> {
    const post = em.create(Post, { title })
    await em.persistAndFlush(post)
    return post
  }

  // Update post with Arg
  // Return null if post to update not found
  @Mutation(() => Post, { nullable: true })
  async updatePost(
    @Arg('id') id: number,
    @Arg('title') title: string,
    // If property is nullable
    // @Arg('title', () => String, { nullable: true }) title: string,

    @Ctx() { em }: MyContext
  ): Promise<Post | null> {
    // Fetch post
    const post = await em.findOne(Post, { id })
    if (!post) {
      return null
    }
    if (typeof title !== 'undefined') {
      post.title = title
      await em.persistAndFlush(post)
    }
    return post
  }

  // Delete post by id
  @Mutation(() => Boolean)
  async deletePost(
    @Arg('id') id: number,
    @Ctx() { em }: MyContext
  ): Promise<boolean> {
    await em.nativeDelete(Post, { id })
    return true
  }
}
