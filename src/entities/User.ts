import { Entity, PrimaryKey, Property } from '@mikro-orm/core'
import { Field, ObjectType } from 'type-graphql'

@ObjectType() // Create graphQL type
@Entity()
export class User {
  @Field() // Expose field
  @PrimaryKey()
  id!: number

  @Field(() => String)
  @Property({ type: 'date' })
  createdAt = new Date()

  @Field(() => String)
  @Property({ type: 'date', onUpdate: () => new Date() })
  updatedAt = new Date()

  @Field()
  @Property({ type: 'text', unique: true })
  username!: string

  // Hidden field
  @Property({ type: 'text' })
  password!: string
}
