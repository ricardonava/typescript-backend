import { Connection, EntityManager, IDatabaseDriver } from '@mikro-orm/core'

// Create EntityManager(em) type as MyContext
export type MyContext = {
  em: EntityManager<any> & EntityManager<IDatabaseDriver<Connection>>
}
